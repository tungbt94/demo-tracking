package com.demo.controller;

import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
		
	@GetMapping({ "/", "/index" })
    public String main(Model model) {
        return "index";
    }
	
	
	@GetMapping("/areaview")
	public String area( Model model, @RequestParam(value = "id",defaultValue = "0") Integer id) {
		log.info("id={}",id);
		switch (id) {
		case 0:
			
			model.addAttribute("val", arrTotal);
			break;
		case 1:
			model.addAttribute("val", arr1);
			break;
		case 2:
			model.addAttribute("val", arr2);
			break;
			
		case 3:
			model.addAttribute("val", arr3);
			break;
		case 4:
			model.addAttribute("val", arr4);
			break;
		default:
			model.addAttribute("val", arrTotal);
			break;
		}
		model.addAttribute("id", id);
		return "areaview";
	}
	
	
	@GetMapping("/detail")
	public String detail(Model model,  @RequestParam(value = "id",defaultValue = "0") Integer id) {
		log.info("id={}",id);
		switch (id) {
		case 0:
			model.addAttribute("val", arrTotal);
			break;
		case 1:
			model.addAttribute("zoneName", "Quầy bán vé");
			model.addAttribute("val", arr1);
			break;
		case 2:
			model.addAttribute("zoneName", "Quảng trường");
			model.addAttribute("val", arr2);
			break;
			
		case 3:
			model.addAttribute("zoneName", "Công viên nước");
			model.addAttribute("val", arr3);
			break;
		case 4:
			model.addAttribute("zoneName", "Khách sạn");
			model.addAttribute("val", arr4);
			break;
		default:
			model.addAttribute("val", arrTotal);
			break;
		}
		model.addAttribute("id", id);
		return "table";
	}
	
	private String[] arrTotal = {"215,000","450,000","58.75","2800"};
	private String[] arr1 = {"55,000","105,000","75","750"};
	private String[] arr2 = {"35,000","65,000","45","450"};
	private String[] arr3 = {"40,000","75,000","55","350"};
	private String[] arr4 = {"85,000","205,000","60","1250"};
	
	private static Logger log = LoggerFactory.getLogger(MainController.class);
}
